import requests
import re

YOPMAIL_URL = 'http://www.yopmail.com/'
YOPMAIL_MAIL_SUFFIX = '@yopmail.fr'

def validation_url(mail):
    session = requests.Session()
    session.headers.update({"User-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0, merluch-pwner"})

    # Necessary: login, yp, yj, v
    # e.g. : login=<mail>
    # yp=FZmV1AmV1AGZ2ZwV1AQH1AmZ
    # yj=YZGD4AGp3ZGH4ZGN1ZwNlZmt
    # v=3.1
    r = session.get(f'{YOPMAIL_URL}inbox.php?login={mail}&yp=QZmp2Zwp4ZwL4ZQL4AGp4AwR&yj=YZGD4AGp3ZGH4ZGN1ZwNlZmt&v=3.1')
    searched = re.search("m\.php\?b.*?\&id=me_.*?==", r.text)
    print(f'Searched: {searched}')
    if searched is None: 
        print('Waiting for mail...')
        return None
    else:
        url = YOPMAIL_URL + searched.group(0)

        # Necessary: b, id
        # e.g. for 'http://www.yopmail.com/m.php?b=dsqdsqfqsf&id=me_ZwNkZGN5ZwN1ZGVmZQNjAwN5AGH1Zj=='
        # b=dsqdsqfqsf (<mail>)
        # id=me_ZwNkZGN5ZwN1ZGVmZQNjAwN5AGH1Zj==
        # cookies: compte=<mail>
        r = session.get(url, cookies={"compte": mail})
        print(r.text)
        url = re.search('(?P<url>https:\/\/a\.noussommespour\.fr\/inscription\/confirmer.+?)"', r.text).group('url')
        return url.replace('amp;', '')

