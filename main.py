import merluch
import tempr
# import yopmail

import requests
import time
import sys
import threading
import uuid


def pwn(first_name, last_name, email, zip_location):
    sid = tempr.create_mail(email)
    print(sid)
    if merluch.register(first_name,
                        last_name,
                        email + tempr.TEMPR_MAIL_SUFFIX,
                        zip_location):
        print(f'[{threading.current_thread().name}] Registered: {first_name} '
              f'{last_name} ({zip_location})'
              f' <{email + tempr.TEMPR_MAIL_SUFFIX}>')
        url = tempr.validation_url(sid, email)
        if url is None:
            return None

        session = requests.Session()
        session.headers.update(
            {'User-agent': ('Mozilla/5.0 (X11; Linux x86_64; rv:45.0) '
                            'Gecko/20100101 Firefox/45.0, merluch-pwner')})
        if session.get(url).status_code == 200:
            print(f'[{threading.current_thread().name}] Validated inscription'
                  f': {first_name} {last_name} ({zip_location}) <'
                  f'{email + tempr.TEMPR_MAIL_SUFFIX}>')
        else:
            print(f'[{threading.current_thread().name}] '
                  f'Failed validation inscription: {first_name} '
                  f'{last_name} ({zip_location}) <'
                  f'{email + tempr.TEMPR_MAIL_SUFFIX}>')


def pwn_routine():
    email = str(uuid.uuid4())[:19] + 'pnwed'
    pwn('Merluch', 'GotPwned', email, 69420)


if __name__ == "__main__":
    nb_thread = 4
    if len(sys.argv) > 1:
        nb_thread = int(sys.argv[1])

    i = 0
    while True:
        if len(threading.enumerate()) < nb_thread:
            t = threading.Thread(None, pwn_routine, f'Thread-{i}')
            t.daemon = True
            t.start()
