import requests
import re
import time
import urllib
import uuid

TEMPR_URL = 'https://tempr.email/'
TEMPR_MAIL_SUFFIX = '@tempr.email'

def create_mail(mail, verbose=False):
    session = requests.Session()
    session.headers.update({'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0, merluch-pwner'})
    sid = str(uuid.uuid4()).replace('-', '')[:16]

    r = session.post(TEMPR_URL, cookies={'DomainId': '1', 'LocalPart': mail, 'sid': sid}, data={
        'LocalPart': mail,
        'DomainType': 'public',
        'DomainId': '1',
        'PrivateDomain': '',
        'Password': '',
        'LoginButton': 'Postfach+abrufen',
        'CopyAndPaste': mail + TEMPR_MAIL_SUFFIX
    })

    if r.status_code == 200 or r.status_code == 302:
        if verbose:
            print('Tempr mail creation <{mail}{TEMPR_MAIL_SUFFIX}> successful.' )
        return sid
    else:
        if verbose:
            print('Tempr mail creation <{mail}{TEMPR_MAIL_SUFFIX}> failed.' )
        return None

def validation_url(sid, mail, verbose=False):
    session = requests.Session()
    session.headers.update({'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0, merluch-pwner',})

    for i in range(0, 30):
        r = session.get(f'{TEMPR_URL}inbox.htm', cookies={'DomainId': '1', 'LocalPart': mail, 'sid': sid})
        searched = re.search("https:\/\/tempr\.email\/message-.+?\.htm", r.text)
        if searched is not None or r.status_code != 200:
            break
        time.sleep(1)
 
    if searched is None:
        return None

    validation_mail_url = searched.group(0)
    if verbose:
        print('Found validation email: {validation_mail_url}')
    r = session.get(validation_mail_url.replace('.htm', '-mailVersion=plain.htm'), cookies={'DomainId': '1', 'LocalPart': mail, 'sid': sid})

    validation_url = re.search('(?P<url>https:\/\/a\.noussommespour\.fr\/inscription\/confirmer.+?)"', r.text).group('url')
    return urllib.parse.unquote(validation_url)
    