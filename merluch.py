
import requests
import json

MERLUCH_URL = 'https://noussommespour.fr/'
REGISTERING_URL = 'https://noussommespour.fr/wp-admin/admin-ajax.php'

def register(first_name, last_name, email, location_zip):
    session = requests.Session()
    session.headers.update({'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0, merluch-pwner'})

    response = session.post(REGISTERING_URL, {
        'post_id': 2,
        'form_id': '9352cee',
        'queried_id': 2,
        'form_fields[first_name]': first_name,
        'form_fields[last_name]': last_name,
        'form_fields[email]': email,
        'form_fields[contact_phone]': '',
        'form_fields[location_zip]': str(location_zip),
        'form_fields[agir_referer]': '',
        'action': 'elementor_pro_forms_send_form',
        'referrer': MERLUCH_URL
    })
    try:
        parsed = json.loads(response.text)
        return parsed['success']
    except:
        return False

if __name__ == "__main__":

    print(register('aaa', 'aaa', 'qsdfghjkl@tempr.email', 69695))